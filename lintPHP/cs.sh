#!/bin/bash
set -e

#linting error
echo "testing the codesniffer"
find . -type f -name '*.php' -o -name '*.inc' > ./filelist
phpcs --file-list=./filelist
